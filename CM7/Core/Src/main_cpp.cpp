#include <cstdio>
#include <iostream>
#include "main.h"
#include "at86rf215.hpp"
#include "at86rf215config.hpp"

namespace AT86RF215 {
AT86RF215 transceiver = AT86RF215(&hspi1, AT86RF215Configuration());
}

extern "C" void main_cpp() {
	AT86RF215::Error error;

	AT86RF215::transceiver.chip_reset(error);
	AT86RF215::transceiver.setup(error);

	uint8_t irq = AT86RF215::transceiver.get_irq(AT86RF215::RF09, error);
	uint16_t sum = 0;

	while (1) {

		HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);
		HAL_Delay(200);

		// Test code for debugging purposes
		uint8_t response = AT86RF215::transceiver.spi_read_8(0x06, error);

		printf("%d", error);

		AT86RF215::transceiver.setup(error);

		uint8_t response_blck[3];
		uint8_t *response_block = AT86RF215::transceiver.spi_block_read_8(0x05,
				3, response_blck, error);
		printf("%d", error);

		response = AT86RF215::transceiver.spi_read_8(0x07, error);

		printf("%d", response);

		uint8_t vn = AT86RF215::transceiver.get_version_number(error);
		AT86RF215::transceiver.set_state(AT86RF215::RF09,
				AT86RF215::State::RF_TRXOFF, error);
		AT86RF215::State state = AT86RF215::transceiver.get_state(
				AT86RF215::RF09, error);
		for (uint8_t i = 0; i < 100; i++) {
			sum += HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_14);
		}

		irq = AT86RF215::transceiver.get_irq(AT86RF215::RF09, error);
		AT86RF215::transceiver.set_state(AT86RF215::RF09,
				AT86RF215::State::RF_SLEEP, error);

		for (uint8_t i = 0; i < 100; i++) {
			sum += HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_14);
		}

		state = AT86RF215::transceiver.get_state(AT86RF215::RF09, error);
		for (uint8_t i = 0; i < 100; i++) {
			sum += HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_14);
		}

		irq = AT86RF215::transceiver.get_irq(AT86RF215::RF09, error);
		AT86RF215::transceiver.set_state(AT86RF215::RF09,
				AT86RF215::State::RF_TRXOFF, error);
		state = AT86RF215::transceiver.get_state(AT86RF215::RF09, error);
		for (uint8_t i = 0; i < 100; i++) {
			sum += HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_14);
		}

		irq = AT86RF215::transceiver.get_irq(AT86RF215::RF09, error);

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief This function handles EXTI line[15:10] interrupts.
 */
extern "C" void EXTI15_10_IRQHandler(void) {
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_14);

	AT86RF215::transceiver.handle_irq();
}
